package org.storm.euler;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Problem_03Test {

    @Test
    void largestPrimeFactorOf() {
        Problem_03 example = new Problem_03();
        assertThat(example.primeFactorsOf(13195), contains(5L, 7L, 13L, 29L));
        assertEquals(29, example.largestPrimeFactorOf(13195));

        Problem_03 p = new Problem_03();
        assertEquals(6857, p.largestPrimeFactorOf(600851475143L), 0);
    }
}