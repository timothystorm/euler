package org.storm.euler;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Problem_07Test {
    private Problem_07 _problem;

    @BeforeEach
    void init() {
        _problem = new Problem_07();
    }

    @Test
    void example() {
        assertEquals(13, _problem.nthPrime(6), 0);
    }

    @Test
    void prime_to_10001() {
        System.out.println(_problem.nthPrime(10_001));
    }
}