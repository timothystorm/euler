package org.storm.euler;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Problem_05Test {

    @Test
    void leastMultiple() {
        Problem_05 p = new Problem_05();
        assertEquals(2520, p.leastMultiple(10), 0);

        System.out.println(p.leastMultiple(20));
        assertEquals(232792560, p.leastMultiple(20));
    }
}