package org.storm.euler;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Problem_06Test {

    @Test
    void example() {
        Problem_06 p = new Problem_06();
        assertEquals(2640, p.sumSquareDiff(10), 0);
    }

    @Test
    void diff_to_100() {
        Problem_06 p = new Problem_06();
        System.out.println(p.sumSquareDiff(100));
    }
}