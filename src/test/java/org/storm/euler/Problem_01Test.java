package org.storm.euler;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Problem_01Test {

    @Test
    void sum_lambda() {
        Problem_01 example = new Problem_01(10, 3, 5);
        assertEquals(example.sum_lambda(), 23);

        Problem_01 p = new Problem_01(1000, 3, 5);
        assertEquals(p.sum_lambda(), 233168, 0);
    }

    @Test
    void sum_mixed() {
        Problem_01 example = new Problem_01(10, 3, 5);
        assertEquals(example.sum_mixed(), 23);

        Problem_01 p = new Problem_01(1000, 3, 5);
        assertEquals(p.sum_mixed(), 233168, 0);
    }

    @Test
    void sum_trad() {
        Problem_01 example = new Problem_01(10, 3, 5);
        assertEquals(example.sum_trad(), 23);

        Problem_01 p = new Problem_01(1000, 3, 5);
        assertEquals(p.sum_trad(), 233168, 0);
    }
}