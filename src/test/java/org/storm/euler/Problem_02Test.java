package org.storm.euler;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Problem_02Test {

    @Test
    void fibonacciEvenSum() {
        Problem_02 example = new Problem_02();
        assertEquals(44, example.fibonacciEvenSum(100), 0);

        Problem_02 p = new Problem_02();
        assertEquals(4613732, p.fibonacciEvenSum(4_000_000), 0);
    }

    @Test
    void fibonacciProof() {
        Problem_02 example = new Problem_02();
        assertEquals(44, example.fibonacciProof(100), 0);

        Problem_02 p = new Problem_02();
        assertEquals(4613732, p.fibonacciProof(4_000_000), 0);
    }

}