package org.storm.euler;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Problem_10Test {
    private Problem_10 _problem;

    @BeforeEach
    public void init() {
        _problem = new Problem_10();
    }

    @Test
    void example() {
        assertEquals(17, _problem.sumOfPrimesLessThan(10), 0);
    }

    @Test
    void sum_below_2_000_000() {
        System.out.println(_problem.sumOfPrimesLessThan(2_000_000));
    }
}