package org.storm.euler;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class Problem_04Test {

    @Test
    void palindromeProduct() {
        Problem_04 example = new Problem_04();
        assertEquals(9009, example.maxPalindromeProduct(2).getProduct(), 0);

        Problem_04 p = new Problem_04();
        Problem_04.MaxPalindrome maxPalindrome = p.maxPalindromeProduct(3);
        assertEquals(906609, maxPalindrome.getProduct(), 0);
        assertThat(maxPalindrome.getFactors(), containsInAnyOrder(993L, 913L));


        System.out.println(p.maxPalindromeProduct(4));
        System.out.println(p.maxPalindromeProduct(5));
        System.out.println(p.maxPalindromeProduct(6));
        System.out.println(p.maxPalindromeProduct(7));
        System.out.println(p.maxPalindromeProduct(8));
    }

    @Test
    void upperLimit() {
        Problem_04 p = new Problem_04();

        assertEquals(9, p.upperLimit(1));
        assertEquals(99, p.upperLimit(2));
        assertEquals(999, p.upperLimit(3));

        try {
            p.upperLimit(-1);
            fail("upper limit should only accept positive integers");
        } catch (AssertionError expected) {
        }
    }
}