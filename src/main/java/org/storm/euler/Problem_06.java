package org.storm.euler;

/**
 * The sum of the squares of the first ten natural numbers is,
 * <p>
 * 12 + 22 + ... + 102 = 385
 * The square of the sum of the first ten natural numbers is,
 * <p>
 * (1 + 2 + ... + 10)2 = 552 = 3025
 * Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.
 * <p>
 * Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
 * <p>
 * Answer: 25,164,150
 */
public class Problem_06 {
    long sumOfSquares(int n) {
        return (n * (n + 1) * (2 * n + 1)) / 6;
    }

    long squareOfSums(int n) {
        return (long) Math.pow(n * (n + 1) / 2, 2);
    }

    public long sumSquareDiff(int max) {
        long sumOfSqures = sumOfSquares(max);
        long squareOfSums = squareOfSums(max);
        return Math.max(sumOfSqures, squareOfSums) - Math.min(sumOfSqures, squareOfSums);
    }
}
