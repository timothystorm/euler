package org.storm.euler;

import java.util.Arrays;

/**
 * 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
 *
 * What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
 */
public class Problem_05 {
    /**
     *
     * @param value
     * @return
     */
    public long leastMultiple(long value){
        if(value > 42) throw new StackOverflowError("value <" + value + "> will result in a term larger than long's MAX");
        if(value < 3) return value;
        return lcm(leastMultiple(value - 1), value);
    }

    private long lcm(long a, long b){
        return (a / gcd(a, b)) * b;
    }

    private long gcd(long a, long b){
        if(b != 0) return gcd(b, a % b);
        return Math.abs(a);
    }
}
