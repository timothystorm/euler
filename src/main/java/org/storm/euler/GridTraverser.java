package org.storm.euler;

import java.io.InputStream;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

public class GridTraverser {
    private final Consumer<int[]> _horz, _vert, _back, _forw;

    private GridTraverser(Consumer<int[]> range) {
        this(range, range, range, range);
    }

    private GridTraverser(Consumer<int[]> horz, Consumer<int[]> vert, Consumer<int[]> back, Consumer<int[]> forw){
        this._horz = horz;
        this._vert = vert;
        this._back = back;
        this._forw = forw;
    }

    public static void main(String[] args) {
        final AtomicLong max = new AtomicLong(1);
        Consumer<int[]> ranger = (range) -> {
            long product = 1;
            for (int i : range) {
                product = product * i;
            }
            if (product > max.get()) max.set(product);
        };

        GridTraverser gt = new GridTraverser(ranger, null, null, null);
        gt.traverse(50, 20, 13);
        System.out.println(max);
    }

    private void traverse(int x, int y, int length) {
        InputStream is = ClassLoader.getSystemResourceAsStream("problem_08.dat");
        Scanner scanner = new Scanner(is);

        // load grid
        int[][] grid = new int[y][x];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                grid[i][j] = scanner.nextInt();
            }
        }

        for (int i = 0; i < y; i++) {
            for (int j = 0; j < x; j++) {
                if (j < (x - length) + 1 && _horz != null) {
                    int[] horz = new int[length];
                    for (int k = 0; k < length; k++) {
                        horz[k] = grid[i][j + k];
                    }
                    _horz.accept(horz);
                }

                if (i < (y - length) + 1 && _vert != null) {
                    int[] vert = new int[length];
                    for (int k = 0; k < length; k++) {
                        vert[k] = grid[i + k][j];
                    }
                    _vert.accept(vert);
                }

                if ((j < (x - length) + 1) && (i < (y - length) + 1) && _back != null) {
                    int[] negDiag = new int[length];
                    for (int k = 0; k < length; k++) {
                        negDiag[k] = grid[i + k][j + k];
                    }
                    _back.accept(negDiag);
                }

                if ((j > length - 2) && (i < (y - length) + 1) && _forw != null) {
                    int[] posDiag = new int[length];
                    for (int k = 0; k < length; k++) {
                        posDiag[k] = grid[i + k][j - k];
                    }
                    _forw.accept(posDiag);
                }
            }
        }
    }
}
