package org.storm.euler;

/**
 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 * <p>
 * Find the sum of all the primes below two million.
 *
 * <p>
 * Answer: 142913828922
 */
public class Problem_10 {
    public long sumOfPrimesLessThan(long max) {
        long sum = 0;
        for (int i = 0; i < max; i++) {
            if (Utils.isPrime(i)) sum += i;
        }
        return sum;
    }
}
