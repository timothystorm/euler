package org.storm.euler;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Scanner;

public class Problem_13 {
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        LambdaBigInteger.main(null);
//        TraditionalBigInteger.main(null);
//        TransposeMatrix.main(null);
        System.out.println(System.currentTimeMillis() - start);
    }

    static class LambdaBigInteger {
        public static void main(String[] args) {
            InputStream is = Utils.classpathResource("problem_13.dat");
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            BigInteger bi = reader.lines()
                    .map(BigInteger::new)
                    .reduce(BigInteger::add)
                    .orElse(BigInteger.ZERO);
            System.out.println(bi);
        }
    }

    static class TraditionalBigInteger {
        public static void main(String[] args) {
            InputStream is = Utils.classpathResource("problem_13.dat");
            Scanner scanner = new Scanner(is);
            BigInteger bi = BigInteger.ZERO;
            while (scanner.hasNext()) {
                bi = bi.add(new BigInteger(scanner.next()));
            }
            System.out.println(bi);
        }
    }

    static class TransposeMatrix {
        public static void main(String[] args) {
            InputStream is = Utils.classpathResource("problem_13.dat");
            Scanner scanner = new Scanner(is);

            // load
            int cols = 0, rows = 0;
            int[][] matrix = new int[0][0];
            while (scanner.hasNext()) {
                int[] row = Arrays.stream(scanner.next()
                        .split(""))
                        .mapToInt(Integer::parseInt)
                        .toArray();

                // swap
                matrix = Arrays.copyOf(matrix, matrix.length + 1);
                matrix[matrix.length - 1] = row;

                // captured dimensions
                rows++;
                cols = Math.max(cols, row.length);
            }

            // transpose
            int[][] transpose = new int[cols][rows];
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < cols; j++) {
                    transpose[j][i] = matrix[i][j];
                }
            }

            // sum back-to-front : smallest to largest place values
            StringBuilder str = new StringBuilder();
            int carry = 0;
            for (int x = cols - 1; x >= 0; x--) {
                int sum = Arrays.stream(transpose[x]).sum();
                sum += carry;

                str.insert(0, (sum % 10));
                carry = sum / 10;
            }

            // reduce carry
            while (carry > 0) {
                str.insert(0, (carry % 10));
                carry = carry / 10;
            }

            System.out.println(str);
        }
    }
}
