package org.storm.euler;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

/**
 * A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
 * <p>
 * Find the largest palindrome made from the product of two 3-digit numbers.
 * </p>
 * <p>
 * Answer: 906609
 * Completed on Fri, 21 Sep 2018, 14:27
 * </p>
 */
public class Problem_04 {
    public MaxPalindrome maxPalindromeProduct(int digits) {
        long upper = upperLimit(digits);
        long lower = (upper / 10) + 1;

        MaxPalindrome max = new MaxPalindrome(0L);
        for (long i = upper; i >= lower; i--) {
            for (long j = i; j >= lower; j--) {
                long product = i * j;
                if (product < max.getProduct()) break;
                if (isPalindrome(product) && (product > max.getProduct())) {
                    max = new MaxPalindrome(product, i, j);
                }
            }
        }
        return max;
    }

    boolean isPalindrome(long number) {
        long copy = number;
        long rev = 0;

        // reverse digits
        while (copy != 0) {
            rev = (rev * 10) + (copy % 10);
            copy /= 10;
        }

        return number == rev;
    }

    long upperLimit(int digits) {
        assert digits > 0;
        return IntStream.range(0, digits)
                .reduce(0, (reduced, i) -> (reduced * 10) + 9);
    }

    public static class MaxPalindrome {
        private final Long _product;
        private final List<Number> _factors;

        public MaxPalindrome(long product, long... factors) {
            _product = product;
            _factors = LongStream.of(factors).boxed().collect(Collectors.toList());
        }

        public long getProduct() {
            return _product;
        }

        public List<Number> getFactors() {
            return _factors;
        }

        @Override
        public String toString() {
            StringBuilder str = new StringBuilder();
            str.append(_product);

            if (_factors.size() > 0) str.append(" = ").append(_factors.get(0));
            if (_factors.size() > 1) str.append(" * ").append(_factors.get(1));
            return str.toString();
        }
    }
}
