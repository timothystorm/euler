package org.storm.euler;

/**
 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
 * <p>
 * What is the 10 001st prime number?
 *
 * <p>
 * Answer: 104743
 * </p>
 */
public class Problem_07 {
    public long nthPrime(int nth) {
        long prime = Long.MIN_VALUE;
        for (long i = 0, count = 0; count < nth; i++) {
            if (Utils.isPrime(i)) {
                count++;
                prime = i;
            }
        }
        return prime;
    }
}
